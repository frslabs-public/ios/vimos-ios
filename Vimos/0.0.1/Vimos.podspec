#
# Be sure to run `pod lib lint Vimos.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Vimos'
  s.version          = '0.0.1'
  s.summary          = 'vimos is used for video call'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/vimos-ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'ashish' => 'ashish@frslabs.com' }
  s.source           = { :http => 'https://vimos-ios.repo.frslabs.space/vimos-ios/0.0.1/Vimos.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '13.0'
  s.ios.vendored_frameworks = 'Vimos.framework'
  s.swift_version = '5.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}


end
